from django.shortcuts import render
from django.http import HttpResponse

from .models import *

# Create your views here.
def escolha(request):
    all_pais = Pais.objects.all()
    all_estado = Estado.objects.all()
    context={'all_pais':all_pais,
             'all_estado':all_estado,
             }
    if request.GET:
        return HttpResponse('<h1> País: ' + request.GET.get('pais') + '<br>Estado: ' + request.GET.get('estado') +
                            '<br><br><a href="/">Voltar</a></h1>')
    return render(request, 'teste/teste.html', context)