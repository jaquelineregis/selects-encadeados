from django.forms import ModelForm
from .models import *

class PaisMF(ModelForm):
    class Meta:
        fields = '__all__'

class EstadoMF(ModelForm):
    class Meta:
        fields = '__all__'