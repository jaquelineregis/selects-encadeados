from django.db import models

# Create your models here.
class Pais(models.Model):
    pais = models.CharField(max_length=50, verbose_name='País')

    class Meta:
        verbose_name = 'País'
        verbose_name_plural = 'Países'

    def __str__(self):
        return self.pais


class Estado(models.Model):
    estado = models.CharField(max_length=50, verbose_name='Estado')
    pais = models.ForeignKey(Pais)

    def __str__(self):
        return self.estado

